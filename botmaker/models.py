from django.db import models

class User(models.Model):
    name=models.CharField(max_length=256)
class Bot(models.Model):
    nm=models.CharField(max_length=256)
    tweet=models.TextField(default="")
    time=models.IntegerField()
class APIkey(models.Model):
    access_token=models.CharField(max_length=256)
    access_token_secret=models.CharField(max_length=256)
