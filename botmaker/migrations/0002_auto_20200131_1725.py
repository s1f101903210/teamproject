# Generated by Django 2.2.5 on 2020-01-31 08:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('botmaker', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='apikey',
            name='con_key',
        ),
        migrations.RemoveField(
            model_name='apikey',
            name='con_secret',
        ),
    ]
