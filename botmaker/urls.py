from django.urls import path
from . import views

urlpatterns=[
    #top
    path(r'',views.top,name='top'),
    #index、データの確認
    path(r'botmaker/',views.index,name='index'),
    #データ作成
    path(r'botmaker/data',views.data, name='data'),
    #詳細画面
    path(r'botmaker/<int:data_id>/',views.detail,name='detail'),
    #Bot作成、関数
    path('botmaker/<int:data_id>/bot',views.bot,name='bot'),
    #Bot作成完了報告
    path(r'botmaker/con/',views.con,name='con'),
    #デリート
    path('botmaker/index/delete', views.delete, name='delete'),
]