from django.shortcuts import render,redirect
from botmaker.models import User
from .models import Bot
from .AutoTweet import f

# Create your views here.
def top(request):
    """top"""
    return render(request,'botmaker/top.html')

def index(request):
    """データの確認"""
    datas=Bot.objects.all()
    data_dictionary={'datas':datas}
    return render(request,'botmaker/index.html',data_dictionary)

def data(request):
    """入力したデータ"""
    if request.method=="POST":
        data=Bot(nm=request.POST['acount'],tweet=request.POST['text'],time=request.POST['time'],)
        data.save()
        return redirect('detail',data.id)
    return render(request, 'botmaker/data.html')

def detail(request, data_id):
    """送られたデータを表示"""
    datas=Bot.objects.get(id=data_id)
    return render(request,'botmaker/detail.html', {'datas' : datas})

def con(request):
    """bot作成完了報告"""
    return render(request,'botmaker/con.html')

def bot(request, data_id):
    datas=Bot.objects.get(id=data_id)
    x=datas.tweet
    result=f(x)
    return redirect('con')

def delete(request):
    try:
        data=Bot.objects.all()
    except Bot.DoesNotExist:
        raise Http404("No")
    data.delete()
    return redirect('index')